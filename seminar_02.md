# Базы данных

**Преподаватель курса**: Сингх-Пальчевская Лавприт

**e-mail**: preet.94.kaur@mail.ru (Тема письма: БД-2022)

## Семинар 2. SQL-запросы

### 1. Теоретическая справка

#### 1.0 Введение

**Операторы SQL:**

<img src="imgs/sem_2/img0_sql_statements.png"  width="500">

**Типы данных SQL:**

<img src="imgs/sem_2/img1_sql_datatypes.png"  width="500">

**Операторы определения данных (Data Defenition Language):**

1. `CREATE` – создание объектов БД
```sql
CREATE [TEMPORARY] TABLE [IF NOT EXISTS] tbl_name(
    col_name_1   datatype_1,
    col_name_2   datatype_2,
    ...
    col_name_N   datatype_N
);
```

2. `ALTER` – модификация объектов БД
```sql
ALTER TABLE table_name ADD column_name datatype;
ALTER TABLE table_name DROP column_name;
ALTER TABLE table_name RENAME column_name TO new_column_name;
ALTER TABLE table_name ALTER column_name TYPE datatype;
...
```

3. `DROP` – удаление объектов БД 
```sql
DROP TABLE [IF EXISTS] table_name;
```

4. `TRUNCATE` – удаление содержимого объекта БД (данные удаляются целым куском, нельзя удалять по условию)
```sql
TRUNCATE TABLE table_name;
```

**Операторы манипуляции данными (Data Manipulation Language):**

1. `SELECT` – выбирает данные, удовлетворяющие заданным условиям
2. `INSERT` – добавляет новые данные
```sql
INSERT INTO table_name [(comma_separated_column_names)] VALUES (comma_separated_values);
```

3. `UPDATE` – изменяет (обновляет) существующие данные
```sql
UPDATE table_name
    SET update_assignment_comma_list
WHERE conditional_experssion;
```

4. `DELETE` – удаляет существующие данные (данные удаляются построчно – можно задавать условие, "откатывать" удаление)
```sql
DELETE
    FROM table_name
[WHERE conditional_expression];
```


#### 1.1 Структура запроса

Порядок написания запроса:

```sql
SELECT [DISTINCT] select_item_comma_list -- список столбцов для вывода
FROM table_reference_comma_list -- список таблиц
[WHERE conditional_expression] -- условия фильтрации, можно использовать AND / OR / NOT
[GROUP BY column_name_comma_list] -- условие группировки
[HAVING conditional_expression] -- условие фильтрации после группировки
[ORDER BY order_item_comma_list]; -- список полей, по которым сортируется вывод
```

#### 1.2 Порядок выполнения запроса

Порядок выполнения запроса отличается от порядка его записи, это необходимо помнить:

**FROM <span>&#8594;</span> WHERE <span>&#8594;</span> GROUP BY <span>&#8594;</span> HAVING <span>&#8594;</span> SELECT <span>&#8594;</span> ORDER BY**

####  1.3 Агрегирующие функции
					
При группировке в блоке `SELECT` могут встречаться либо атрибуты, по которым происходит группировка, либо атрибуты, которые подаются на вход агрегирующим функциям. В SQL есть 5 стандартных агрегирующих функций. При выполнении запроса функции не учитывается специальное значение `NULL`, которым обозначается отсутствующее значение.
					
* `count()` – количество записей с известным значением. Если необходимо подсчитать количество уникальных значений, можно использовать `count(DISTINCT field_nm)`
* `max()` - наибольшее из всех выбранных значений поля
* `min()` - наименьшее из всех выбранных значений поля
* `sum()` - сумма всех выбранных значений поля
* `avg()` - среднее всех выбранных значений поля

					 					
#### 1.4 Операции соединения таблиц (JOIN)
					
Операции соединения делятся на 3 группы:
					
* `CROSS JOIN` - декартово произведение 2 таблиц
* `INNER JOIN` - соединение 2 таблиц по условию. В результирующую выборку попадут только те записи, которые удовлетворяют условию соединения
* `OUTER JOIN` - соединение 2 таблиц по условию. В результирующую выборку могут попасть записи, которые не удовлетворяют условию соединения: 
    * `LEFT (OUTER) JOIN` - все строки "левой" таблицы попадают в итоговую выборку
    * `RIGHT (OUTER) JOIN` - все строки "правой" таблицы попадают в итоговую выборку 
    * `FULL (OUTER) JOIN` - все строки обеих таблиц попадают в итоговую выборку

<img src="imgs/sem_2/img3_sql_join.png"  width="500">

#### 1.5 Полезные функции

Иногда бывает полезно использовать в запросе специальные функции:
* `in` - принадлежность определенному набору значений:
`X in (a1, a2, ..., an)` <span>&#8803;</span> X = a<sub>1</sub> or X = a<sub>2</sub> or ... or X = a<sub>n</sub>
* `between` - принадлежность определенному интервалу значений:
`X between A and B` <span>&#8803;</span> (X >= A and X <= B) or (X <= A and X >= B)
* `like` - удовлетворение текста паттерну: `X like '0%abc_0'`, где `_` - ровно 1 символ, а `%` - любая последовательность символов (в том числе нулевой длины).

### 2. Практическое задание

Решения можно исполнять в [SQLize](https://sqlize.online/sql/psql14/9cffb8e3d397e93627eb41cd55b10c20/).

1. Создать схему seminar_2:

```sql
create schema seminar_2;
```

2. Создать таблицу salary в новой схеме с полями `employee_nm, department_nm, salary`. Какого типа должны быть поля? Какой должна быть размерность этих полей?

3. Проверить, что таблица создана, написав запрос к системной таблице `information_schema.tables`.

4. Заполнить таблицу 5 тестовыми строками.

5. Добавить в таблицу новое поле `comment`.

6. Написать запрос для обновления поля с комментарием. Для каждой строки необходимо указать свой комментарий. Подумайте, как это сделать одной операций `UPDATE`, а не пятью разными запросами.

7. Удалить одну из строк таблицы на выбор.

8. Очистить таблицу, используя оператор группы DDL.

9. Удалить из таблицы столбец с комментарием.

10. Запустить запросы со вставками данных, полученные от семинариста.

```sql
INSERT INTO seminar_2.salary VALUES('Ken Sánchez', 'HR', 78);
INSERT INTO seminar_2.salary VALUES('TerriLee Duffy', 'HR', 95);
INSERT INTO seminar_2.salary VALUES('Roberto Tamburello', 'HR', 382);
INSERT INTO seminar_2.salary VALUES('Rob Walters', 'HR', 16);
INSERT INTO seminar_2.salary VALUES('Gail Erickson', 'HR', 1079);
INSERT INTO seminar_2.salary VALUES('Jossef Gibson', 'HR', 102);
INSERT INTO seminar_2.salary VALUES('Dylan Miller', 'HR', 486);
INSERT INTO seminar_2.salary VALUES('Diane Margheim', 'HR', 1953);
INSERT INTO seminar_2.salary VALUES('Gigi Matthew', 'SALE', 49);
INSERT INTO seminar_2.salary VALUES('Michael Raheem', 'SALE', 71);
INSERT INTO seminar_2.salary VALUES('Ovidiu Cracium', 'SALE', 94);
INSERT INTO seminar_2.salary VALUES('Thierry Hers', 'SALE', 61);
INSERT INTO seminar_2.salary VALUES('Janice Galvin', 'SALE', 972);
INSERT INTO seminar_2.salary VALUES('Michael Sullivan', 'SALE', 849);
INSERT INTO seminar_2.salary VALUES('Sharon Salavaria', 'SALE', 715);
INSERT INTO seminar_2.salary VALUES('David Michael', 'SALE', 94);
INSERT INTO seminar_2.salary VALUES('Kevin Brown', 'R&D', 891);
INSERT INTO seminar_2.salary VALUES('John Wood', 'R&D', 1486);
INSERT INTO seminar_2.salary VALUES('Mary Dempsey', 'R&D', 176);
INSERT INTO seminar_2.salary VALUES('Wanida Benshoof', 'R&D', 49);
INSERT INTO seminar_2.salary VALUES('Terry Eminhizer', 'R&D', 381);
INSERT INTO seminar_2.salary VALUES('Sariya Harnpadoungsataya', 'R&D', 946);
INSERT INTO seminar_2.salary VALUES('Mary Gibson', 'R&D', 486);
INSERT INTO seminar_2.salary VALUES('Jill Williams', 'R&D', 19);
INSERT INTO seminar_2.salary VALUES('James Hamilton', 'R&D', 46);
INSERT INTO seminar_2.salary VALUES('Peter Krebs', 'R&D', 445);
INSERT INTO seminar_2.salary VALUES('Jo Brown', 'R&D', 666) ;
INSERT INTO seminar_2.salary VALUES('Guy Gilbert', 'MANAGEMENT', 482);
INSERT INTO seminar_2.salary VALUES('Mark McArthur', 'MANAGEMENT', 12);
INSERT INTO seminar_2.salary VALUES('Britta Simon', 'MANAGEMENT', 194);
INSERT INTO seminar_2.salary VALUES('Margie Shoop', 'MANAGEMENT', 481);
INSERT INTO seminar_2.salary VALUES('Rebecca Laszlo', 'MANAGEMENT', 16);
INSERT INTO seminar_2.salary VALUES('Annik Stahl', 'MANAGEMENT', 134);
INSERT INTO seminar_2.salary VALUES('Suchitra Mohan', 'R&D', 87);
INSERT INTO seminar_2.salary VALUES('Brandon Heidepriem', 'R&D', 111) ;
INSERT INTO seminar_2.salary VALUES('Jose Lugo', 'R&D', 185);
INSERT INTO seminar_2.salary VALUES('Chris Okelberry', 'R&D', 94);
INSERT INTO seminar_2.salary VALUES('Kim Abercrombie', 'R&D', 348);
INSERT INTO seminar_2.salary VALUES('Ed Dudenhoefer', 'R&D', 68);
INSERT INTO seminar_2.salary VALUES('JoLynn Dobney', 'R&D', 346);
INSERT INTO seminar_2.salary VALUES('Bryan Baker', 'R&D', 185);
INSERT INTO seminar_2.salary VALUES('James Kramer', 'SUPPORT', 965);
INSERT INTO seminar_2.salary VALUES('Nancy Anderson', 'SUPPORT', 444);
INSERT INTO seminar_2.salary VALUES('Simon Rapier', 'SUPPORT', 133);
INSERT INTO seminar_2.salary VALUES('Thomas Michaels', 'SUPPORT', 200);
INSERT INTO seminar_2.salary VALUES('Eugene Kogan', 'SUPPORT', 144);
INSERT INTO seminar_2.salary VALUES('Andrew Hill', 'SUPPORT', 186);
INSERT INTO seminar_2.salary VALUES('Ruth Ellerbrock', 'SUPPORT', 179);
INSERT INTO seminar_2.salary VALUES('Barry Johnson', 'HEAD', 10000);
INSERT INTO seminar_2.salary VALUES('Sidney Higa', 'HEAD', 1);
INSERT INTO seminar_2.salary VALUES('Max Lanson', 'PR', 150);
```

11. Вывести все значения из таблицы `salary`.

12. Найти всех сотрудников с зарплатой выше 500.

13. Найти всех сотрудников с зарплатой ниже 400 из отдела HR.

14. Найти всех сотрудников, зарплата которых лежит в диапазоне от 300 до 500.

15. Найти всех сорудников, в имени (не фамилии) которых встречается буква 'A', с зарплатой не менее 100. Считаем, что имя и фамилия разделены одним пробелом.

16. Вывести всех сотрудников из отделов HR и PR, не успользуя оператор `OR`.

17. Найти все отделы, названия которых состоят ровно из 2 символов. Вывести каждый отдел ровно 1 раз.

18. Найти все отделы, названия которых начинаются на букву 'S'. Вывести каждый отдел столько раз, сколько он встречается в таблице.

19. Вывести всех сотрудников, которые не работают в отделах 'R&D' и 'SUPPORT', не используя операторы `OR` и `AND`.

20. Вывести все отделы в названии которых встречаются буквы 'A' и 'E'. Порядок следования букв в названии значения не имеет.

21. Найти количество строк в таблице `salary`.

22. Найти максимальную зарплату по всем сотрудникам.

23. Посчитать число сотрудников с зарплатой выше 500.

24. Найти среднюю зарплату в разбивке по отделам.

25. Найти все отделы со средней зарплатой свыше 300.

26. Найти все отделы с минимальной зарплатой свыше 500.

27. Найти все отделы с числом сотрудников не менее 10 человек.

28. Вывести список всех сострудников, отсортированный по убыванию зарплаты.

29. Вывести список всех отделов по убыванию средней зарплаты в отделе, а также среднюю, минимальную, максимальную зарплаты и количество человек в отделе.

30. Подсчитать отклонение максимальной зарплаты по всех отделах от минимальной и средней.

31. Для каждого отдела рассчитать отклонепние максимальной зарплаты от средней и минимальной, а также отклонение средней от минимальной.

32. Вывести отделы по возрастанию фонда оплаты труда.
